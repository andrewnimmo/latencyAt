package latencyAt

import "time"

// Config keeps platform configuration.
type Config struct {
	// TokenLength is the length of Prometheus auth tokens
	TokenLength int

	// Signing key for incoming webhook requests from stripe
	StripeHookSigningKey string

	// Username and password for internal endpoints on public API
	AuthUsername string
	AuthPassword string

	EmailTokenLength int
	EmailTokenMaxAge time.Duration
}

var DefaultConfig = &Config{
	TokenLength:      32,
	EmailTokenLength: 32,
	EmailTokenMaxAge: 24 * time.Hour,
}
