package mock

import "gitlab.com/latency.at/latencyAt"

type EmailTokenService struct {
	CreateEmailTokenFn func(t *latencyAt.EmailToken) error
}

func (s *EmailTokenService) EmailToken(id int) (*latencyAt.EmailToken, error) {
	panic("not implemented")
}

func (s *EmailTokenService) EmailTokenByToken(token string) (*latencyAt.EmailToken, error) {
	panic("not implemented")
}

func (s *EmailTokenService) CreateEmailToken(t *latencyAt.EmailToken) error {
	return s.CreateEmailTokenFn(t)
}

func (s *EmailTokenService) UpdateEmailToken(t *latencyAt.EmailToken) error {
	panic("not implemented")
}

func (s *EmailTokenService) DeleteEmailToken(t *latencyAt.EmailToken) error {
	panic("not implemented")
}

func (s *EmailTokenService) Purge() error {
	return nil
}

func (s *EmailTokenService) Healthy() error {
	return nil
}
