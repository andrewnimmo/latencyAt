package subs

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/Sirupsen/logrus"
	log "github.com/Sirupsen/logrus"
	"github.com/prometheus/client_golang/prometheus"
	stripe "github.com/stripe/stripe-go"
	"github.com/stripe/stripe-go/card"
	"github.com/stripe/stripe-go/customer"
	"github.com/stripe/stripe-go/invoice"
	"github.com/stripe/stripe-go/plan"
	"github.com/stripe/stripe-go/sub"

	"gitlab.com/latency.at/latencyAt"
	"gitlab.com/latency.at/latencyAt/errors"
)

var (
	requestCounter = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Namespace: "lat",
			Subsystem: "stripe",
			Name:      "requests_total",
			Help:      "Total number of requests by handler",
		},
		[]string{"method", "path"},
	)
	requestDuration = prometheus.NewHistogramVec(
		prometheus.HistogramOpts{
			Namespace: "lat",
			Subsystem: "stripe",
			Name:      "request_duration_seconds",
			Help:      "Duration of request by handler histogram",
			Buckets:   prometheus.ExponentialBuckets(0.01, 5, 3),
		},
		[]string{"method", "path"},
	)
	stripeErrorCounter = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Namespace: "lat",
			Subsystem: "stripe",
			Name:      "errors_total",
			Help:      "Total number of error when talking to stripe",
		},
		[]string{"method", "path"},
	)
	eventCounter = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Namespace: "lat",
			Subsystem: "stripe",
			Name:      "events_received_total",
			Help:      "Total number of stripe events received, by type",
		},
		[]string{"type"},
	)
	eventProcessedCounter = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Namespace: "lat",
			Subsystem: "stripe",
			Name:      "events_processed_total",
			Help:      "Total number of stripe events processed, by type",
		},
		[]string{"type"},
	)
	eventPendingGauge = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Namespace: "lat",
			Subsystem: "stripe",
			Name:      "events_pending",
			Help:      "Currently pending events/webhooks, by type",
		},
		[]string{"type"},
	)
	eventErrorCounter = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Namespace: "lat",
			Subsystem: "stripe",
			Name:      "event_errors_total",
			Help:      "Total number of errors when processing events",
		},
		[]string{"type"},
	)
	freePlan = &stripe.Plan{
		ID:     "free",
		Amount: 0,
		Name:   "Free",
		Meta: map[string]string{
			"requests": "50000",
		},
	}
)

func init() {
	prometheus.MustRegister(requestCounter)
	prometheus.MustRegister(requestDuration)
	prometheus.MustRegister(stripeErrorCounter)
	prometheus.MustRegister(eventCounter)
	prometheus.MustRegister(eventProcessedCounter)
	prometheus.MustRegister(eventPendingGauge)
	prometheus.MustRegister(eventErrorCounter)

	be := &backend{
		&stripe.BackendConfiguration{
			Type:       stripe.APIBackend,
			URL:        stripe.APIURL,
			HTTPClient: http.DefaultClient,
		},
	}
	stripe.SetBackend("api", be)
	stripe.Logger = logrus.New()
}

// backend implements stripe.Backend
type backend struct {
	Backend *stripe.BackendConfiguration
}

func firstDir(path string) string {
	for _, p := range strings.SplitN(path, "/", 3) {
		if p == "" {
			continue
		}
		return "/" + p
	}
	return ""
}

func (b *backend) Call(method, path, key string, body *stripe.RequestValues, params *stripe.Params, v interface{}) error {
	start := time.Now()
	lvs := []string{method, firstDir(path)}
	requestCounter.WithLabelValues(lvs...).Inc()
	if err := b.Backend.Call(method, path, key, body, params, v); err != nil {
		stripeErrorCounter.WithLabelValues(lvs...).Inc()
		return err
	}
	requestDuration.WithLabelValues(lvs...).Observe(time.Since(start).Seconds())
	return nil
}

func (b *backend) CallMultipart(method, path, key, boundary string, body io.Reader, params *stripe.Params, v interface{}) error {
	start := time.Now()
	lvs := []string{method, firstDir(path)}
	requestCounter.WithLabelValues(lvs...).Inc()
	if err := b.Backend.CallMultipart(method, path, key, boundary, body, params, v); err != nil {
		stripeErrorCounter.WithLabelValues(lvs...).Inc()
		return err
	}
	requestDuration.WithLabelValues(lvs...).Observe(time.Since(start).Seconds())
	return nil
}

type SubscriptionService struct {
	latencyAt.UserService
	latencyAt.EventService
	VatRates map[string]float64
}

func NewSubscriptionService(us latencyAt.UserService, es latencyAt.EventService, vatRatesFilename string) (*SubscriptionService, error) {
	rates, err := parseVatRates(vatRatesFilename)
	if err != nil {
		return nil, err
	}
	return &SubscriptionService{
		UserService:  us,
		EventService: es,
		VatRates:     rates,
	}, nil
}

func (s *SubscriptionService) GetTax(cc string) (*latencyAt.VatRate, error) {
	country := strings.ToUpper(cc)
	rate, ok := s.VatRates[country]
	return &latencyAt.VatRate{Rate: rate, InEU: ok}, nil
}

// FIXME: Fugly
func (s *SubscriptionService) getTax(user *latencyAt.User, card *stripe.Card) (float64, error) {
	// Check that we have a country set, just in case this can happen.
	if card.CardCountry == "" {
		return 0.0, errors.ErrCardNoCountry
	}

	taxPercent := 0.0
	if user.VatIn == "" {
		if tax, ok := s.VatRates[card.CardCountry]; ok {
			taxPercent = tax * 100
		}
	}
	return taxPercent, nil
}

func (s *SubscriptionService) Subscribe(userID int, planName string) error {
	// Subscribing to free plan = unsubscribing from existing plan
	if planName == freePlan.ID {
		return s.Unsubscribe(userID)
	}
	user, err := s.UserService.User(userID)
	if err != nil {
		return err
	}
	cparams := &stripe.CustomerParams{}
	cparams.Expand("default_source")
	cust, err := customer.Get(user.StripeCustomerID, cparams)
	if err != nil {
		return err
	}
	if cust.DefaultSource == nil {
		return errors.ErrCardNotFound
	}
	taxPercent, err := s.getTax(user, cust.DefaultSource.Card)
	if err != nil {
		return err
	}
	// customer.DefaultSource.Card.CardCountry
	params := &stripe.SubParams{
		Customer:   user.StripeCustomerID,
		Plan:       planName,
		TaxPercent: taxPercent,
		NoProrate:  true, // Disable prorate for downgrades
	}
	params.AddMeta("userid", strconv.Itoa(user.ID))
	params.AddMeta("vatin", user.VatIn)
	params.AddMeta("country", cust.DefaultSource.Card.CardCountry)

	switch n := len(cust.Subs.Values); n {
	case 0:
		// New subscription
		_, err := sub.New(params)
		if err != nil {
			return err
		}
	case 1:
		// Update subscription
		subs := cust.Subs.Values[0]
		if planName == subs.Plan.Name {
			return fmt.Errorf("Already subscribed to %s", planName)
		}
		plans, err := s.Plans()
		if err != nil {
			return fmt.Errorf("Couldn't retrieve plans: %s", err)
		}
		plan, ok := plans[planName]
		if !ok {
			return fmt.Errorf("Couldn't find plan %s. Available plans: %v", planName, plans)
		}
		reqsNew, err := requestFromPlan(plan)
		if err != nil {
			return err
		}
		reqsOld, err := requestFromPlan(subs.Plan)
		if err != nil {
			return err
		}
		if reqsNew > reqsOld {
			// On Upgrade we reset billing interval to now
			params.TrialEndNow = true

			// Enable prorate
			params.NoProrate = false
		}
		_, err = sub.Update(subs.ID, params)
		if err != nil {
			return err
		}
	default:
		// Invalid state
		return errors.ErrSubsNumberInvalid
	}
	return nil
}

func (s *SubscriptionService) Unsubscribe(userID int) error {
	user, err := s.UserService.User(userID)
	if err != nil {
		return err
	}
	if user.StripeCustomerID == "" {
		return errors.ErrCustomerNotFound
	}
	c, err := customer.Get(user.StripeCustomerID, nil)
	if err != nil {
		return err
	}
	if len(c.Subs.Values) == 0 {
		return errors.ErrSubsNotFound
	}
	_, err = sub.Cancel(c.Subs.Values[0].ID, nil)
	return err
}

func (s *SubscriptionService) Plans() (map[string]*stripe.Plan, error) {
	i := plan.List(&stripe.PlanListParams{})
	plans := map[string]*stripe.Plan{
		"free": freePlan,
	}
	for i.Next() {
		plan := i.Plan()
		plans[plan.ID] = plan
	}
	return plans, i.Err()
}

func (s *SubscriptionService) GetSubscription(customerID string) (*stripe.Sub, error) {
	c, err := customer.Get(customerID, nil)
	if err != nil {
		return nil, err
	}
	if c.Subs == nil {
		return &stripe.Sub{Plan: freePlan}, nil
	}

	n := len(c.Subs.Values)
	switch {
	case n > 1:
		return nil, errors.ErrSubsNumberInvalid
	case n == 1:
		return c.Subs.Values[0], nil
	}
	return &stripe.Sub{Plan: freePlan}, nil
}

func (s *SubscriptionService) ProcessPayment(invoice *stripe.Invoice) error {
	var subscription *stripe.Sub

	switch invoice.Meta["upgrade"] {
	case "true":
		// upgrade invoices contain line items with references to old and new sub
		// so we loop over them to figure out the active one.

		// var plan *stripe.Plan
		for _, it := range invoice.Lines.Values {
			if it.Sub == "" {
				return fmt.Errorf("upgrade: Line item misses subscription: %#v", it)
			}
			s, err := sub.Get(it.Sub, nil)
			if err != nil {
				return err
			}
			if s.Status != "active" {
				continue
			}
			if subscription != nil {
				if subscription.ID == s.ID {
					continue
				}
				return fmt.Errorf("Found more than one active subscription")
			}
			subscription = s
		}
		if subscription == nil {
			return fmt.Errorf("Found no active subscriptions in invoice: %#v", invoice.Lines.Values)
		}
	default:
		// Regular (initial and recurring) invoices have a direct Sub member
		var err error
		subscription, err = sub.Get(invoice.Sub, nil)
		if err != nil {
			return err
		}
	}

	requests, err := requestFromPlan(subscription.Plan)
	if err != nil {
		return err
	}

	if invoice.Meta["upgrade"] == "true" {
		return s.UserService.BalanceByStripeCustomerID(invoice.Customer.ID, requests)
	}
	return s.UserService.BalanceAbsByStripeCustomerID(invoice.Customer.ID, requests)
}

func (s *SubscriptionService) HandleEvent(event *stripe.Event) error {
	eventCounter.WithLabelValues(event.Type).Inc()
	eventPendingGauge.WithLabelValues(event.Type).Set(float64(event.Webhooks))

	logger := log.WithFields(log.Fields{"eventType": event.Type, "eventID": event.ID})
	_, err := s.EventService.Get(event.ID)
	if err == nil {
		logger.Info("Already processed event, skipping")
		return nil
	}
	if err != errors.ErrEventNotFound {
		eventErrorCounter.WithLabelValues(event.Type).Inc()
		return err
	}
	eventProcessedCounter.WithLabelValues(event.Type).Inc()

	jsn, err := json.Marshal(event)
	if err != nil {
		eventErrorCounter.WithLabelValues(event.Type).Inc()
		return err
	}
	levent := &latencyAt.Event{
		Key:   event.ID,
		Value: string(jsn),
	}
	switch event.Type {
	case "customer.subscription.updated":
		// We get this event for 'started new billing cycle' but without 'items'
		if event.Data.Prev["items"] == nil {
			break
		}
		reqStr, ok := event.Data.Prev["items"].(map[string]interface{})["data"].([]interface{})[0].(map[string]interface{})["plan"].(map[string]interface{})["metadata"].(map[string]interface{})["requests"].(string)
		if !ok {
			eventErrorCounter.WithLabelValues(event.Type).Inc()
			return errors.ErrSubsInvalidData
		}
		requestsOld, err := strconv.Atoi(reqStr)
		if err != nil {
			eventErrorCounter.WithLabelValues(event.Type).Inc()
			return fmt.Errorf("requests in previous plan metadata not a number: %s", err)
		}

		subs := &stripe.Sub{}
		if err := subs.UnmarshalJSON(event.Data.Raw); err != nil {
			eventErrorCounter.WithLabelValues(event.Type).Inc()
			return err
		}
		requests, err := requestFromPlan(subs.Plan)
		if err != nil {
			eventErrorCounter.WithLabelValues(event.Type).Inc()
			return err
		}

		lf := log.Fields{
			"customer":    subs.Customer.ID,
			"requests":    requests,
			"requestsOld": requestsOld,
		}

		if requests > requestsOld {
			logger.WithFields(lf).Info("Subscription updated: Upgrading customer")

			cparams := &stripe.CustomerParams{}
			cparams.Expand("default_source")
			cust, err := customer.Get(subs.Customer.ID, cparams)
			if err != nil {
				return err
			}
			user, err := s.UserService.UserByEmail(cust.Email)
			if err != nil {
				return err
			}

			if cust.DefaultSource == nil {
				return errors.ErrCardNotFound
			}
			taxPercent, err := s.getTax(user, cust.DefaultSource.Card)
			if err != nil {
				return err
			}

			params := &stripe.InvoiceParams{
				Customer:   subs.Customer.ID,
				TaxPercent: taxPercent,
			}
			params.AddMeta("upgrade", "true")
			params.AddMeta("userid", strconv.Itoa(user.ID))
			params.AddMeta("vatin", user.VatIn)
			params.AddMeta("country", cust.DefaultSource.Card.CardCountry)

			tx, err := s.EventService.AddTx(levent)
			if err != nil {
				eventErrorCounter.WithLabelValues(event.Type).Inc()
				return err
			}
			_, err = invoice.New(params)
			if err != nil {
				if rerr := tx.Rollback(); rerr != nil {
					logger.WithFields(lf).Warn("Couldn't rollback transaction: ", rerr)
				}
				eventErrorCounter.WithLabelValues(event.Type).Inc()
				return fmt.Errorf("Couldn't generate invoice: %s", err)
			}
			return tx.Commit()
		}
		logger.WithFields(lf).Debug("Subscription updated")

	case "invoice.created":
		iv := &stripe.Invoice{}
		if err := iv.UnmarshalJSON(event.Data.Raw); err != nil {
			eventErrorCounter.WithLabelValues(event.Type).Inc()
			return err
		}
		if iv.Paid {
			break
		}
		tx, err := s.EventService.AddTx(levent)
		if err != nil {
			eventErrorCounter.WithLabelValues(event.Type).Inc()
			return err
		}
		if _, err := invoice.Pay(iv.ID, nil); err != nil {
			if rerr := tx.Rollback(); rerr != nil {
				logger.WithFields(log.Fields{
					"customer": iv.Customer.ID,
				}).Warn("Couldn't rollback transaction: ", rerr)
			}
			eventErrorCounter.WithLabelValues(event.Type).Inc()
			return fmt.Errorf("Couldn't pay invoice: %s", err)
		}
		return tx.Commit()

	case "invoice.payment_succeeded":
		invoice := &stripe.Invoice{}
		if err := invoice.UnmarshalJSON(event.Data.Raw); err != nil {
			eventErrorCounter.WithLabelValues(event.Type).Inc()
			return err
		}
		tx, err := s.EventService.AddTx(levent)
		if err != nil {
			eventErrorCounter.WithLabelValues(event.Type).Inc()
			return err
		}
		if err := s.ProcessPayment(invoice); err != nil {
			if rerr := tx.Rollback(); rerr != nil {
				logger.WithFields(log.Fields{
					"customer": invoice.Customer.ID,
				}).Warn("Couldn't rollback transaction: ", rerr)
			}
			eventErrorCounter.WithLabelValues(event.Type).Inc()
			return fmt.Errorf("Couldn't process the payment: %s", err)
		}
		return tx.Commit()
	}
	return nil
}

func (s *SubscriptionService) AddCard(userID int, token string) error {
	user, err := s.UserService.User(userID)
	if err != nil {
		return err
	}
	if user.StripeCustomerID == "" {
		params := &stripe.CustomerParams{
			Email: user.Email,
		}
		c, err := customer.New(params)
		if err != nil {
			return err
		}

		if err := s.UserService.SetStripeCustomerID(user.ID, c.ID); err != nil {
			return err
		}
		user.StripeCustomerID = c.ID
	}
	c, err := card.New(&stripe.CardParams{
		Customer: user.StripeCustomerID,
		Token:    token,
		Default:  true,
	})

	_, err = customer.Update(user.StripeCustomerID, &stripe.CustomerParams{DefaultSource: c.ID})
	if err != nil {
		return fmt.Errorf("addCard: Couldn't set new card default: %s", err)
	}
	return nil
}

func (s *SubscriptionService) GetCard(userID int) (*stripe.Card, error) {
	user, err := s.UserService.User(userID)
	if err != nil {
		return nil, err
	}
	if user.StripeCustomerID == "" {
		return nil, &errors.Error{Err: errors.ErrCardNotFound, StatusCode: http.StatusNotFound}
	}
	c, err := customer.Get(user.StripeCustomerID, nil)
	if c.DefaultSource == nil {
		return nil, &errors.Error{Err: errors.ErrCardNotFound, StatusCode: http.StatusNotFound}
	}
	// FIXME: Customer should include card but go-stripe doesn't unmarshal
	// it. See: https://github.com/stripe/stripe-go/issues/398
	return card.Get(c.DefaultSource.ID, &stripe.CardParams{Customer: c.ID})
}

func (s *SubscriptionService) DeleteCard(userID int) error {
	user, err := s.UserService.User(userID)
	if err != nil {
		return err
	}

	if user.StripeCustomerID == "" {
		return &errors.Error{Err: errors.ErrCardNotFound, StatusCode: http.StatusNotFound}
	}
	c, err := customer.Get(user.StripeCustomerID, nil)
	if len(c.Subs.Values) > 0 {
		return &errors.Error{Err: errors.ErrCardDeleteSubActive, StatusCode: http.StatusConflict}
	}
	if c.DefaultSource == nil {
		return &errors.Error{Err: errors.ErrCardNotFound, StatusCode: http.StatusNotFound}
	}
	_, err = card.Del(c.DefaultSource.ID, &stripe.CardParams{Customer: c.ID})
	return err
}

func requestFromPlan(plan *stripe.Plan) (int, error) {
	rs, ok := plan.Meta["requests"]
	if !ok {
		return 0, fmt.Errorf("No requests found for plan %s", plan.Name)
	}
	requests, err := strconv.Atoi(rs)
	if err != nil {
		return 0, fmt.Errorf("Requests field is not a number but: %s", rs)
	}
	return requests, nil
}
