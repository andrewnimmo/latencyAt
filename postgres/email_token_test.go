package postgres

import (
	"reflect"
	"testing"
	"time"

	"gitlab.com/latency.at/latencyAt"
)

func TestNewEmailTokenService(t *testing.T) {
	db, err := initDB()
	assert(t, err)

	es, err := NewEmailTokenService(db)
	assert(t, err)
	assert(t, es.Purge())
	defer es.Purge()

	et := &latencyAt.EmailToken{
		ID:      0, // gets filled by CreateEmailToken()
		Token:   "foobar23425",
		Created: time.Now().Truncate(time.Millisecond).In(time.UTC),
	}

	for _, step := range []func(e *latencyAt.EmailToken) error{
		func(e *latencyAt.EmailToken) error {
			return es.CreateEmailToken(et)
		},
		func(e *latencyAt.EmailToken) error {
			et.Token = "blafasel42"
			return es.UpdateEmailToken(et)
		},
	} {
		if err := step(et); err != nil {
			t.Fatal(err)
		}

		ret, err := es.EmailToken(et.ID)
		if err != nil {
			t.Fatal(err)
		}
		ret.Created = ret.Created.In(et.Created.Location())
		if !reflect.DeepEqual(et, ret) {
			t.Fatalf("\na> %#v\nb> %#v", et, ret)
		}
	}
	assert(t, es.DeleteEmailToken(et))
	if ret, err := es.EmailToken(et.ID); err == nil {
		t.Fatalf("Expected error but found token: %v", ret)
	}
}
