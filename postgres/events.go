package postgres

import (
	"github.com/jmoiron/sqlx"

	"gitlab.com/latency.at/latencyAt"
	"gitlab.com/latency.at/latencyAt/errors"
)

const EventServiceSchema = `
CREATE TABLE IF NOT EXISTS events (
  key varchar(255) UNIQUE,
  value text,
  timestamp timestamp
);
`

// Ensure EventService implements latencyAt.EventService.
var _ latencyAt.EventService = &EventService{}

type EventService struct {
	db            *sqlx.DB
	stmtSelect    *sqlx.Stmt
	stmtInsert    *sqlx.Stmt
	stmtDelete    *sqlx.Stmt
	stmtDeleteAll *sqlx.Stmt
}

func NewEventService(db *sqlx.DB) (*EventService, error) {
	es := &EventService{db: db}
	for ref, str := range map[**sqlx.Stmt]string{
		&es.stmtSelect:    "SELECT key, value, timestamp FROM events WHERE key = $1",
		&es.stmtInsert:    "INSERT INTO events (key, value, timestamp) VALUES ($1, $2, $3)",
		&es.stmtDeleteAll: "DELETE FROM events",
	} {
		s, err := db.Preparex(str)
		if err != nil {
			return nil, err
		}
		*ref = s
	}
	return es, nil
}

func (s *EventService) Healthy() error {
	return s.db.Ping()
}

func (s *EventService) Purge() error {
	_, err := s.stmtDeleteAll.Exec()
	return err
}

func (s *EventService) Get(key string) (*latencyAt.Event, error) {
	rows, err := s.stmtSelect.Query(key)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	if !rows.Next() {
		return nil, errors.ErrEventNotFound
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	event := &latencyAt.Event{}
	return event, rows.Scan(&event.Key, &event.Value, &event.Timestamp)
}

func (s *EventService) Add(event *latencyAt.Event) error {
	_, err := s.stmtInsert.Exec(&event.Key, &event.Value, &event.Timestamp)
	return err
}

func (s *EventService) AddTx(event *latencyAt.Event) (latencyAt.Tx, error) {
	tx, err := s.db.Beginx()
	if err != nil {
		return nil, err
	}
	stmt := tx.Stmtx(s.stmtInsert)
	_, err = stmt.Exec(&event.Key, &event.Value, &event.Timestamp)
	if err != nil {
		return nil, err
	}
	return tx, nil
}
