package http

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/julienschmidt/httprouter"
	stripe "github.com/stripe/stripe-go"
	"github.com/stripe/stripe-go/webhook"

	"gitlab.com/latency.at/latencyAt"
	"gitlab.com/latency.at/latencyAt/errors"
)

func (a *APIHandler) handleGetPlans(r *http.Request, _ httprouter.Params) (interface{}, error) {
	return a.SubscriptionService.Plans()
}

func (a *APIHandler) handleUserSubscription(r *http.Request, _ httprouter.Params, userID int) (interface{}, error) {
	user, err := a.UserService.User(userID)
	if err != nil {
		return nil, err
	}
	if user.StripeCustomerID == "" {
		return map[string]string{"subscription": ""}, nil
	}
	sub, err := a.SubscriptionService.GetSubscription(user.StripeCustomerID)
	if err != nil {
		return nil, err
	}
	return map[string]*stripe.Sub{"subscription": sub}, nil
}

func (a *APIHandler) handleAddCard(r *http.Request, _ httprouter.Params, userID int) (interface{}, error) {
	token := stripe.Token{}
	if err := json.NewDecoder(r.Body).Decode(&token); err != nil {
		return nil, err
	}

	if err := a.SubscriptionService.AddCard(userID, token.ID); err != nil {
		return nil, err
	}
	return &latencyAt.StatusOK, nil
}

func (a *APIHandler) handleGetCard(r *http.Request, _ httprouter.Params, userID int) (interface{}, error) {
	return a.SubscriptionService.GetCard(userID)
}

// FIXME: What about a middleware that injects User directly and handles returned errors?
func (a *APIHandler) handleDeleteCard(r *http.Request, _ httprouter.Params, userID int) (interface{}, error) {
	if err := a.SubscriptionService.DeleteCard(userID); err != nil {
		return nil, err
	}
	return &latencyAt.StatusOK, nil
}

func (a *APIHandler) handleUserUnsubscribe(r *http.Request, _ httprouter.Params, userID int) (interface{}, error) {
	if err := a.SubscriptionService.Unsubscribe(userID); err != nil {
		if err == errors.ErrSubCustomerNotFound || err == errors.ErrSubNotFound {
			return nil, &errors.Error{Err: err, StatusCode: http.StatusNotFound}
		}
		return nil, err
	}
	return &latencyAt.StatusOK, nil
}

func (a *APIHandler) handleSubscribePlan(r *http.Request, ps httprouter.Params, userID int) (interface{}, error) {
	plan := ps.ByName("plan")
	if err := a.SubscriptionService.Subscribe(userID, plan); err != nil {
		return nil, err
	}
	return &latencyAt.StatusOK, nil
}

func (a *APIHandler) stripeHook(r *http.Request, _ httprouter.Params) (interface{}, error) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return nil, fmt.Errorf("Couldn't read webhook request: %s", err)
	}
	header := r.Header.Get("Stripe-Signature")
	event, err := webhook.ConstructEvent(body, header, a.Config.StripeHookSigningKey)
	if err != nil {
		return nil, err
	}
	if err := a.SubscriptionService.HandleEvent(&event); err != nil {
		return nil, err
	}
	return &latencyAt.StatusOK, nil
}

func (a *APIHandler) handleGetTax(r *http.Request, ps httprouter.Params, userID int) (interface{}, error) {
	country := ps.ByName("country")
	return a.SubscriptionService.GetTax(country)
}
